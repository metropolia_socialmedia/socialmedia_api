## Preperation

Before getting the source code you should :

 - Have visual studio installed
 - Installed the .net core 2.2 or higher
 - have MSQL server installed

When you have this you can download the source code.

 Before the project works you must create a database with MSQL server and import the given file in the main directory.

When the project is open.
Create an Appsettings.json at this location of the api : Project API\SocialMedia.API\appsettings.json

In the json there has to be this:

  

    {
	   "ConnectionStrings": {
		"ConnectionString": "CHANGE THIS TO CONNECTION STRING"
	    },
	    "Logging": {
		"LogLevel": {
		    "Default": "Warning"
		}
	    },
	    "AllowedHosts": "*"
    }

  

Change "**CHANGE THIS TO CONNECTION STRING**" to the connection string of your microsoft server.

after this is done, the api should build and run on your pc!

however you should use a proxy to access the api from your phone.

