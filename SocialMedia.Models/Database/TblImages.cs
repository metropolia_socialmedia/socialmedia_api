﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Models
{
    public partial class TblImages
    {
        public TblImages()
        {
            TblPosts = new HashSet<TblPosts>();
        }

        public Guid ImageId { get; set; }
        public string Image { get; set; }

        public virtual ICollection<TblPosts> TblPosts { get; set; }
        public virtual ICollection<tblUsersImages> UsersImages { get; set; }
        public virtual ICollection<tblImageTags> ImageTags { get; set; }
    }
}
