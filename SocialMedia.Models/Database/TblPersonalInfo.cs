﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Models
{
    public partial class TblPersonalInfo
    {
        public Guid PersonalInfoId { get; set; }
        public string InfoType { get; set; }
        public string Value { get; set; }
        public bool IsPublic { get; set; }
        public virtual ICollection<tblUsersInfo> TblUsersInfo { get; set; }

    }
}
