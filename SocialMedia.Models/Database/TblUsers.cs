﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Models
{
    public partial class TblUsers
    {
        public TblUsers()
        {
            TblTags = new HashSet<TblTags>();
        }

        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid? Token { get; set; }
        public DateTime TokenEndTime { get; set; } = DateTime.Now;
        public Guid? Refreshtoken { get; set; }
        public DateTime RefreshtokenEndTime { get; set; } = DateTime.Now;

        public virtual ICollection<TblTags> TblTags { get; set; }
        public virtual ICollection<tblUsersInfo> TblUsersInfo { get; set; }
        public virtual ICollection<tblUserPosts> UserPosts { get; set; }
        public virtual ICollection<tblUsersImages> UsersImages { get; set; }
        public virtual ICollection<TblFriends> Friends { get; set; }
    }
}
