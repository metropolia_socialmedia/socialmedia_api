﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Models
{
    public class tblUsersImages
    {
        public Guid UserId { get; set; }
        [Key]
        public Guid ImageId { get; set; }
        public virtual TblUsers User { get; set; }
        public virtual TblImages Images { get; set; }

    }
}
