﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Models
{
    public partial class TblTags
    {
        public Guid TagId { get; set; }
        public Guid UserId { get; set; }

        public virtual TblUsers User { get; set; }
        public virtual ICollection<tblImageTags> ImageTags { get; set; }
    }
}
