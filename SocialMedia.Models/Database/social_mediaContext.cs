﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace SocialMedia.Models
{
    public partial class social_mediaContext : DbContext, Isocial_mediaContext
    {
        public social_mediaContext(IConfiguration configuration)
        {
            _config = configuration;
        }
        IConfiguration _config;

        public social_mediaContext(DbContextOptions<social_mediaContext> options, IConfiguration configuration)
            : base(options)
        {
            _config = configuration;
        }
        public async Task<int> SaveData()
        {
            return await this.SaveChangesAsync();
        }
        public virtual DbSet<TblImages> TblImages { get; set; }
        public virtual DbSet<TblPersonalInfo> TblPersonalInfo { get; set; }
        public virtual DbSet<TblPosts> TblPosts { get; set; }
        public virtual DbSet<TblTags> TblTags { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }
        public virtual DbSet<tblUserPosts> TblUserPosts { get; set; }
        public virtual DbSet<tblUsersImages> TblUsersImages { get; set; }
        public virtual DbSet<tblUsersInfo> TblUsersInfo { get; set; }
        public virtual DbSet<tblImageTags> TblImageTags { get; set; }
        public virtual DbSet<TblFriends> TblUserFriends { get; set; }
        public static readonly LoggerFactory MyConsoleLoggerFactory
            = new LoggerFactory(new[] {
              new ConsoleLoggerProvider((category, level)
                => category == DbLoggerCategory.Database.Command.Name
               && level == LogLevel.Information, true) });


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLoggerFactory(MyConsoleLoggerFactory).UseSqlServer(_config.GetConnectionString("DefaultConnection"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TblImages>(entity =>
            {
                entity.HasKey(e => e.ImageId);

                entity.ToTable("tblImages");

                entity.Property(e => e.ImageId)
                    .HasColumnName("imageId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Image).IsRequired();
            });

            modelBuilder.Entity<TblPersonalInfo>(entity =>
            {
                entity.HasKey(e => e.PersonalInfoId);

                entity.ToTable("tblPersonalInfo");

                entity.Property(e => e.PersonalInfoId).ValueGeneratedNever();

                entity.Property(e => e.InfoType)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsPublic).HasColumnName("isPublic");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPosts>(entity =>
            {
                entity.HasKey(e => e.PostId);

                entity.ToTable("tblPosts");

                entity.Property(e => e.PostId)
                    .HasColumnName("postId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Imageid).HasColumnName("imageid");

                entity.Property(e => e.PostedAt).HasColumnType("datetime2");

                entity.Property(e => e.Text)
                    .HasColumnName("text")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.TblPosts)
                    .HasForeignKey(d => d.Imageid)
                    .HasConstraintName("FK_tblPosts_tblImages");
            });

            modelBuilder.Entity<TblTags>(entity =>
            {
                entity.HasKey(e => e.TagId);

                entity.ToTable("tblTags");

                entity.Property(e => e.TagId)
                    .HasColumnName("tagId")
                    .ValueGeneratedNever();

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblTags)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tblTags_tblUsers");
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("tblUsers");

                entity.HasIndex(e => new { e.Email, e.Username })
                    .HasName("IX_tblUsers")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .IsRequired()
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .HasColumnName("firstname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .IsUnicode(false);

                entity.Property(e => e.Refreshtoken).HasColumnName("refreshtoken");

                entity.Property(e => e.RefreshtokenEndTime)
                    .HasColumnName("refreshtokenEndTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.TokenEndTime)
                    .HasColumnName("tokenEndTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<tblUsersInfo>(entity =>
            {
                entity.HasKey(e=>e.ID);
                entity.Property(e => e.ID).IsUnicode(false);
                entity.Property(e => e.UserId).IsUnicode(false).HasMaxLength(20);
                entity.Property(e => e.PersonalInfoId).IsUnicode(false).HasMaxLength(20);
                entity.HasOne(e => e.TblUsers)
                .WithMany(m => m.TblUsersInfo)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tblUsersInfo_tblUsers");

                entity.HasOne(e => e.TblPersonalInfo)
                .WithMany(m => m.TblUsersInfo)
                .HasForeignKey(d => d.PersonalInfoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_tblUsersInfo_tblPersonalInfo");
            });
            modelBuilder.Entity<tblUserPosts>(entity =>
            {
                entity.Property(e => e.UserId).IsUnicode(false).HasMaxLength(20);
                entity.Property(e => e.PostId).IsUnicode(false).HasMaxLength(20);
                entity.HasOne(e => e.Users)
                .WithMany(m => m.UserPosts)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblUserPosts_tblUsers");

                entity.HasOne(e => e.Posts)
                .WithMany(m => m.Posts)
                .HasForeignKey(d => d.PostId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblUserPosts_tblPosts");
            });
            modelBuilder.Entity<tblUsersImages>(entity =>
            {
                entity.Property(e => e.ImageId).IsUnicode(false).HasMaxLength(20);
                entity.Property(e => e.UserId).IsUnicode(false).HasMaxLength(20);

                entity.HasOne(e => e.User)
                .WithMany(m => m.UsersImages)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblUsersImages_tblUsers");

                entity.HasOne(e => e.Images)
                .WithMany(m => m.UsersImages)
                .HasForeignKey(d => d.ImageId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblImageTags_tblImages");

            });
            modelBuilder.Entity<TblFriends>(entity =>
            {
                entity.Property(e => e.id).IsUnicode(false);
                entity.Property(e => e.userid).IsUnicode(false).HasMaxLength(20);
                entity.Property(e => e.friendid).IsUnicode(false).HasMaxLength(20);
                entity.HasKey(e => e.id);
                entity.HasOne(e => e.User)
                .WithMany(m => m.Friends)
                .HasForeignKey(d => d.friendid)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblUserFriends_tblUsers1");
            });
            modelBuilder.Entity<tblImageTags>(entity =>
            {
                entity.Property(e => e.ImageId).IsUnicode(false).HasMaxLength(20);
                entity.Property(e => e.TagId).IsUnicode(false).HasMaxLength(20);

                entity.HasOne(e => e.Images)
                .WithMany(m => m.ImageTags)
                .HasForeignKey(d => d.ImageId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblImageTags_tblImages");

                entity.HasOne(e => e.Tags)
                .WithMany(m => m.ImageTags)
                .HasForeignKey(d => d.TagId)
                .OnDelete(DeleteBehavior.SetNull)
                .HasConstraintName("FK_tblImageTags_tblTags");

            });
        }
    }
}
