﻿using System;
using System.Collections.Generic;

namespace SocialMedia.Models
{
    public partial class TblPosts
    {
        public Guid PostId { get; set; }
        public string Text { get; set; }
        public Guid? Imageid { get; set; }
        public DateTime PostedAt { get; set; }

        public virtual TblImages Image { get; set; }
        public virtual ICollection<tblUserPosts> Posts { get; set; }
    }
}
