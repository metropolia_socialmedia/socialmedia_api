﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Models
{
    public class TblFriends
    {
        public int id { get; set; }
        public Guid userid { get; set; }
        public Guid friendid { get; set; }
        public virtual TblUsers User { get; set; }
    }
}
