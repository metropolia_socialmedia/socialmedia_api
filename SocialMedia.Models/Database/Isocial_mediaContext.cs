﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SocialMedia.Models
{
    public interface Isocial_mediaContext
    {
        DbSet<TblFriends> TblUserFriends { get; set; }
        DbSet<TblImages> TblImages { get; set; }
        DbSet<tblImageTags> TblImageTags { get; set; }
        DbSet<TblPersonalInfo> TblPersonalInfo { get; set; }
        DbSet<TblPosts> TblPosts { get; set; }
        DbSet<TblTags> TblTags { get; set; }
        DbSet<tblUserPosts> TblUserPosts { get; set; }
        DbSet<TblUsers> TblUsers { get; set; }
        DbSet<tblUsersImages> TblUsersImages { get; set; }
        DbSet<tblUsersInfo> TblUsersInfo { get; set; }

        Task<int> SaveData();
    }
}