﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Models
{
    public class tblUsersInfo
    {
        public int ID { get; set; }
        public Guid UserId { get; set; }
        public Guid PersonalInfoId { get; set; }
        public virtual TblUsers TblUsers { get; set; }
        public virtual TblPersonalInfo TblPersonalInfo { get; set; }

    }
}
