﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.Models
{
    public class tblUserPosts
    {
        public Guid UserId { get; set; }
        [Key]
        public Guid PostId { get; set; }
        public virtual TblUsers Users { get; set; }
        public virtual TblPosts Posts { get; set; }

    }
}
