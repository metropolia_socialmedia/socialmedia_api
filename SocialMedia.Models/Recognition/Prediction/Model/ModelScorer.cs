﻿using Microsoft.ML;
using Microsoft.ML.Core.Data;
using SocialMedia.Models.Recognition.Prediction.ImageData;
using System;
using System.IO;
using SocialMedia.Models;
using System.Linq;

namespace SocialMedia.Models.Recognition.Prediction.Model
{
    public class ModelScorer
    {
        private readonly string modelLocation;
        private readonly MLContext mlContext;

        public ModelScorer()
        {
            this.modelLocation = AppDomain.CurrentDomain.BaseDirectory + "model.zip";//modelLocation;
            mlContext = new MLContext(1);
        }
        public string ClassifyImage(string dataUrl)
        {
            ConsoleHelper.ConsoleWriteHeader("Loading model");
            Console.WriteLine($"Model loaded: {modelLocation}");
            string endresult = null;
            using (var f = new FileStream(modelLocation, FileMode.Open))
            {
                var loadedModel = mlContext.Model.Load(f);

                var predictor = mlContext.Model.CreatePredictionEngine<ImageNetData, ImageNetPrediction>(loadedModel);
                ImageNetData data = new ImageNetData() { ImagePath = dataUrl, Label = "Minju" };
                var result = new { data, pred = predictor.Predict(data) };
                ConsoleHelper.ConsoleWriteHeader("Making classifications");
                ConsoleHelper.ConsoleWriteImagePrediction(dataUrl, result.pred.PredictedLabelValue, result.pred.Score.Max(), data.Label);
                if (result.pred.Score.Max() >= 0.50)
                {
                    endresult = result.pred.PredictedLabelValue;
                }
                predictor.Dispose();     
            }
            return endresult;
        }

    }
}
