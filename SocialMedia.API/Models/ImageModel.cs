﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Models
{
    public class ImageModel
    {
        public string ImgId { get; set; }
        public string imageUrl { get; set; }
        public string base64 { get; set; }
    }
}
