﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SocialMedia.API.Models
{
    public class UserModel
    {
        public string userid { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public DateTime tokenEndTime { get; set; }
        public string refreshtoken { get; set; }
        public DateTime refreshtokenEndTime { get; set; }
        public string password { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    }
}
