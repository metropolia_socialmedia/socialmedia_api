﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Models
{
    public class PostModel
    {
        public string postid { get; set; }
        public UserModel Owner { get; set; }
        public string text { get; set; }
        public string img { get; set; }
        public DateTime Postedat { get; set; }

    }
}
