﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Models
{
    public class ProfileModel
    {
        public List<string[]> values { get; set; } = new List<string[]>();
        public List<bool> SucceedList { get; set; } = new List<bool>();
    }
}
