﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Models
{
    public class FriendModel
    {
        public int Id { get; set; } = -1;
        public UserModel User { get; set; }
        public ProfileModel Profile { get; set; }
    }
}
