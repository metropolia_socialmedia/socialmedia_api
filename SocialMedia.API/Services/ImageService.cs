﻿using SocialMedia.API.Models;
using SocialMedia.API.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class ImageService : IImageService
    {
        IImageRepo _iRepo;
        public ImageService(IImageRepo iRepo)
        {
            _iRepo = iRepo;
        }
        public async Task<ImageModel> addImage(string userId, ImageModel model)
        {
            if (Guid.TryParse(userId, out Guid userGuid) && model != null && !string.IsNullOrWhiteSpace(model.base64))
            {
                return await _iRepo.addImageAsync(userGuid, model.base64);
            }
            return null;
        }

        public async Task<List<ImageModel>> GetImages(string userid)
        {
            if (Guid.TryParse(userid, out Guid userGuid))
            {
                return await _iRepo.GetImages(userGuid);
            }
            return null;
        }

        public async Task<int> RemoveImage(string userid, string imageid)
        {
            if(Guid.TryParse(userid,out Guid UserGuid)&&Guid.TryParse(imageid,out Guid ImageId))
            {
                return await _iRepo.DeleteImage(UserGuid, ImageId);
            }
            return -1;
        }
    }
}
