﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IPostService
    {
        Task<PostModel> AddPostAsync(string userId, PostModel post);
        Task<int> DeletePost(string postid);
        Task<List<PostModel>> GetPostsAsync(string userId, int count = 25, int page = 0);
        Task<List<PostModel>> GetUserPostsAsync(string userid, int max, int page);
    }
}