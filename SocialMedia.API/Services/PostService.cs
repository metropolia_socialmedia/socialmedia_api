﻿using SocialMedia.API.Models;
using SocialMedia.API.Repos;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class PostService : IPostService
    {
        IFriendRepo _frepo;
        IPostRepo _prepo;
        IImageService _iService;
        public PostService(IFriendRepo friendRepo, IPostRepo postRepo,IImageService iService)
        {
            _frepo = friendRepo;
            _prepo = postRepo;
            _iService = iService;
        }

        public async Task<List<PostModel>> GetPostsAsync(string userId, int count = 25, int page = 0)
        {
            var friends = await _frepo.GetUsersAsync(userId);
            return await _prepo.GetPostsAsync(friends, count, page,userId);
        }
        public async Task<PostModel> AddPostAsync(string userId, PostModel post)
        {
            if (Guid.TryParse(userId, out Guid userGuid))
            {
                var tblpost = new TblPosts { Text = post.text, PostedAt = post.Postedat };
                if (post.img != null)
                {
                    var img = await _iService.addImage(userId, new ImageModel { base64 = post.img });
                    tblpost.Image = new TblImages {ImageId=Guid.Parse(img.ImgId),Image=img.imageUrl };

                }
              
                return await _prepo.AddPostAsync(userGuid, tblpost);
            }
            return null;
        }
        public async Task<int> DeletePost(string postid)
        {
            if (Guid.TryParse(postid, out Guid id))
            {
                return await _prepo.DeletePostAsync(id);
            }
            return -1;
        }

        public async Task<List<PostModel>> GetUserPostsAsync(string userid, int max, int page)
        {
            return await _prepo.GetPostsAsync(new List<FriendModel>(), max, page, userid);
        }
    }
}
