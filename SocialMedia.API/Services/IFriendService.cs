﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IFriendService
    {
        Task<FriendModel> AddFriendAsync(string userid, string friendid);
        Task<int> DeleteFriendAsync(string userId, string friendId);
        Task<List<FriendModel>> GetUserAsync(string id);
        Task<FriendModel> GetPublicProfile(string id);
    }
}