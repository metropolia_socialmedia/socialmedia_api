﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IImageService
    {
        Task<ImageModel> addImage(string userId, ImageModel model);
        Task<List<ImageModel>> GetImages(string userid);
        Task<int> RemoveImage(string userid, string imageid);
    }
}