﻿using SocialMedia.API.Models;
using SocialMedia.API.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class FriendService : IFriendService
    {
        IFriendRepo _fRepo;
        IProfileRepo _pRepo;
        public FriendService(IFriendRepo repo, IProfileRepo profileRepo)
        {
            _fRepo = repo;
            _pRepo = profileRepo;
        }

        public async Task<List<FriendModel>> GetUserAsync(string id)
        {
            var Friends = await _fRepo.GetUsersAsync(id);
            foreach (var item in Friends)
            {
                item.Profile = await _pRepo.GetUserProfile(item.User.userid);
            }
            return Friends;
        }
        public async Task<FriendModel> AddFriendAsync(string userid, string friendid)
        {
            var friend = await _fRepo.AddFriendAsync(userid, friendid);
            friend.Profile = await _pRepo.GetUserProfile(friend.User.userid);
            return friend;
        }
        public async Task<int> DeleteFriendAsync(string userId, string friendId)
        {
            if (Guid.TryParse(userId, out Guid userGuid) && Guid.TryParse(friendId, out Guid friendGuid))
            {
                return await _fRepo.RemoveFriendAsync(userGuid, friendGuid);
            }
            return -1;
        }

        public async Task<FriendModel> GetPublicProfile(string id)
        {
            var friend =await _fRepo.GetPublicUserById(id);
            friend.Profile = await _pRepo.GetUserProfile(id);
            return friend;
        }
    }
}
