﻿using SocialMedia.API.Models;
using SocialMedia.Models.Recognition.Learning.ModelBuilder;
using SocialMedia.Models.Recognition.Prediction.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class RecognitionService : IRecognitionService
    {
        public RecognitionService()
        {
        }
        public async Task<bool> ProcessLearning(List<ImageModel> images, string uid)
        {
            try
            {
                foreach (var image in images)
                {
                    await addImageAsync(uid, image.base64);
                }
                var learner = new ModelBuilder(AppDomain.CurrentDomain.BaseDirectory + "/AiImages/");
                learner.BuildAndTrainMore();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        int i = 0;
        public async Task<string> GetUser(ImageModel image, string uid)
        {
            try
            {
                var Dir = AppDomain.CurrentDomain.BaseDirectory + "/queue/" + uid.ToLower() + ".png";
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/queue/"))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/queue/");
                }
                await File.WriteAllBytesAsync(Dir, Convert.FromBase64String(image.base64));
                var scorer = new ModelScorer();
                return scorer.ClassifyImage(Dir);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "";
            }
        }
        private async Task addImageAsync(string UserId, string Base64)
        {

            var Dir = AppDomain.CurrentDomain.BaseDirectory + "/AiImages/";
            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }
            await File.WriteAllBytesAsync(Dir + UserId.ToLower() + i + ".png", Convert.FromBase64String(Base64));
            using (var f = File.AppendText(Dir + "tags.tsv"))
            {
                f.WriteLine(UserId.ToLower() + i + ".png\t" + UserId.ToLower());
            }
            i++;
        }
    }
}
