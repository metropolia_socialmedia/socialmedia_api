﻿using System.Threading.Tasks;
using SocialMedia.API.Controllers;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IProfileService
    {
        Task<ProfileModel> GetProfile(string userid, string profileid = null);
        Task<ProfileModel> AddProfile(string userid, ProfileModel info);
        Task<ProfileModel> UpdateProfile(string userid, ProfileModel info);
        Task<bool> DeleteProfile(string userid, string profileInfoID);
    }
}