﻿using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IUserService
    {
        Task<string> CheckToken(string token);
        Task<bool> CheckToken(string userid,string token);
        Task<UserModel> LoginUser(string login, string password);
        Task<UserModel> RefreshToken(string id, string token);
        Task<UserModel> RegisterUser(UserModel newUserModel);
    }
}