﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Services
{
    public interface IRecognitionService
    {
        Task<string> GetUser(ImageModel image, string uid);
        Task<bool> ProcessLearning(List<ImageModel> images, string uid);
    }
}