﻿using SocialMedia.API.Models;
using SocialMedia.API.Repos;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class ProfileService : IProfileService
    {
        IProfileRepo _repo;
        public ProfileService(IProfileRepo profile)
        {
            _repo = profile;
        }

        public async Task<ProfileModel> AddProfile(string userid, ProfileModel info)
        {
            var list = new List<TblPersonalInfo>();
            var infolist = new List<tblUsersInfo>();
            foreach (var value in info.values)
            {
                var ProfileInfo = new TblPersonalInfo()
                {
                    PersonalInfoId = Guid.NewGuid(),
                    InfoType = value[0],
                    Value = value[1],
                    IsPublic = value[2] == "true" ? true : false
                };
                list.Add(ProfileInfo);
                infolist.Add(new tblUsersInfo() { UserId = Guid.Parse(userid), PersonalInfoId = ProfileInfo.PersonalInfoId });
            }

            return await _repo.AddProfiles(list, infolist);
        }

        public async Task<bool> DeleteProfile(string userid, string profileInfoID)
        {
            return await _repo.DeleteProfile(userid, profileInfoID);
        }

        public async Task<ProfileModel> GetProfile(string userid, string profileid = null)
        {
            bool check = false;
            if (profileid == null || profileid == userid)
            {
                profileid = userid;
                check = true;
            }
            return await _repo.GetUserProfile(userid, check);
        }

        public async Task<ProfileModel> UpdateProfile(string userid, ProfileModel info)
        {
            var list = new List<TblPersonalInfo>();
            var infolist = new List<tblUsersInfo>();
            List<bool> Succeeded = new List<bool>();
            foreach (var value in info.values)
            {
                if (Guid.TryParse(value[3], out Guid g))
                {
                    var ProfileInfo = new TblPersonalInfo()
                    {
                        PersonalInfoId = g,
                        InfoType = value[0],
                        Value = value[1],
                        IsPublic = value[2] == "true" ? true : false
                    };
                    list.Add(ProfileInfo);
                    infolist.Add(new tblUsersInfo() { UserId = Guid.Parse(userid), PersonalInfoId = ProfileInfo.PersonalInfoId });
                    Succeeded.Add(true);
                }
                else
                {
                    Succeeded.Add(false);
                }

            }

            return await _repo.UpdateProfiles(list, infolist,Succeeded);
        }
    }
}
