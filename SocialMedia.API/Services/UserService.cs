﻿using SocialMedia.API.Models;
using SocialMedia.API.Repos;
using SocialMedia.Models;
using SocialMedia.Models.Security;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialMedia.API.Services
{
    public class UserService : IUserService
    {
        IUserRepo _repo;
        public UserService(IUserRepo smContext)
        {
            _repo = smContext;
        }

        public async Task<UserModel> LoginUser(string login, string password)
        {
            List<UserModel> result;
            if (IsEmail(login))
            {
                result = await _repo.GetUserByEmail(login);

            }
            else
            {
                result = await _repo.GetUserByUsername(login);
            }
            if (result != null)
            {
                UserModel user = null;
                foreach (var item in result)
                {
                    if (Hashing.ComputeSha512Hash(password) ==item.password)
                    {
                        user = item;
                        user.token = Guid.NewGuid().ToString();
                        user.tokenEndTime = DateTime.Now.AddHours(UserRepo.TOKEN_TIME);
                        user.refreshtoken = Guid.NewGuid().ToString();
                        user.refreshtokenEndTime = DateTime.Now.AddHours(UserRepo.REFRESH_TOKEN_TIME);
                        await UpdateUserInfo(user);
                    }
                }
                if (user == null)
                {
                    return null;
                }
                user.password = null;
                return user;
            }
            return null;
        }
        public async Task<string> CheckToken(string token)
        {
            return await _repo.HasValidToken(token);
        }
        public async Task<bool> CheckToken(string id, string token)
        {
            return await _repo.HasValidToken(id, token);
        }
        public async Task<UserModel> RefreshToken(string id, string token)
        {
            var user = await _repo.ExtendTokens(id, token);
            if (user == null)
            {
                return null;
            }
            user.username = null;
            user.email = null;
            return user;
        }
        public async Task<UserModel> RegisterUser(UserModel newUserModel)
        {
            var newUser = new TblUsers
            {
                UserId = Guid.NewGuid(),
                Email = newUserModel.email,
                Firstname = newUserModel.firstname,
                Refreshtoken = Guid.NewGuid(),
                RefreshtokenEndTime = DateTime.Now.AddHours(UserRepo.REFRESH_TOKEN_TIME),
                Token = Guid.NewGuid(),
                TokenEndTime = DateTime.Now.AddHours(UserRepo.TOKEN_TIME),
                Lastname = newUserModel.lastname,
                Password = Hashing.ComputeSha512Hash(newUserModel.password),
                Username = newUserModel.username
            };
            var User = await _repo.RegisterUser(newUser);
            if (User == null)
            {
                return null;
            }
            return new UserModel
            {
                userid = User.UserId.ToString(),
                username = User.Username,
                email = User.Email,
                refreshtoken = User.Refreshtoken.ToString(),
                refreshtokenEndTime = User.RefreshtokenEndTime,
                token = User.Token.ToString(),
                tokenEndTime = User.TokenEndTime,
            };
        }

        private async Task<bool> UpdateUserInfo(UserModel user)
        {
            return await _repo.UpdateUser(user);
        }

        private bool IsEmail(string login)
        {
            return login.Contains("@");
        }
    }
}
