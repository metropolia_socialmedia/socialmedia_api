﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;
using SocialMedia.Models;

namespace SocialMedia.API.Repos
{
    public interface IProfileRepo
    {
        Task<ProfileModel> GetUserProfile(string userid, bool isUser = false);
        Task<ProfileModel> AddProfiles(List<TblPersonalInfo> info,List<tblUsersInfo> usersInfos);
        Task<bool> DeleteProfile(string userid, string profileInfoID);
        Task<ProfileModel> UpdateProfiles(List<TblPersonalInfo> list, List<tblUsersInfo> infolist, List<bool> succeeded);
    }
}