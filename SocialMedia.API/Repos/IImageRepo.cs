﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Repos
{
    public interface IImageRepo
    {
        Task<ImageModel> addImageAsync(Guid UserId, string Base64);
        Task<List<ImageModel>> GetImages(Guid userGuid);
        Task<int> DeleteImage(Guid userGuid, Guid imageId);
    }
}