﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.API.Models;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Repos
{
    public class ImageRepo : IImageRepo
    {
        const string IMAGEENPOINT = "http://192.168.137.1:52490/images/";
        string Dir = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "../../../../SocialMedia.Web/wwwroot/images/").ToString();
        Isocial_mediaContext _context;
        public ImageRepo(Isocial_mediaContext context)
        {
            _context = context;
        }
        public async Task<ImageModel> addImageAsync(Guid UserId, string Base64)
        {
            var imgid = Guid.NewGuid();
            if (!Directory.Exists(Dir + imgid.ToString().ToLower() + "/"))
            {
                Directory.CreateDirectory(Dir + imgid.ToString().ToLower() + "/");
            }
            await File.WriteAllBytesAsync(Dir+ imgid.ToString().ToLower()+"/" + imgid.ToString().ToLower() + ".png", Convert.FromBase64String(Base64));
            await _context.TblImages.AddAsync(new TblImages { ImageId = imgid, Image = IMAGEENPOINT+ imgid.ToString().ToLower() + "/" + imgid.ToString().ToLower() + ".png" });
            await _context.TblUsersImages.AddAsync(new tblUsersImages { ImageId = imgid, UserId = UserId });
            await _context.SaveData();
            return new ImageModel { ImgId = imgid.ToString().ToLower(), imageUrl = IMAGEENPOINT + imgid.ToString().ToLower() + "/" + imgid.ToString().ToLower() + ".png" };
        }

        public async Task<int> DeleteImage(Guid userGuid, Guid imageId)
        {
            var userimage = await _context.TblUsersImages.AsNoTracking().Where(ui => ui.ImageId == imageId && ui.UserId == userGuid).Include(ui=>ui.Images).FirstOrDefaultAsync();
            var img = userimage.Images;
            _context.TblUsersImages.Remove(userimage);
            _context.TblImages.Remove(img);
            File.Delete(Dir + img.ImageId.ToString().ToLower() + ".png");
            return await _context.SaveData();
        }

        public async Task<List<ImageModel>> GetImages(Guid userGuid)
        {
            return await _context.TblUsersImages.Where(ui => ui.UserId == userGuid).Include(ui => ui.Images).Select(ui => new ImageModel { ImgId = ui.ImageId.ToString().ToLower(), imageUrl = ui.Images.Image }).ToListAsync();
        }
        
    }
}
