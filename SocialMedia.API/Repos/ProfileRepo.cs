﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.API.Models;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Repos
{
    public class ProfileRepo : IProfileRepo
    {
        Isocial_mediaContext _context;
        public ProfileRepo(Isocial_mediaContext socialContext)
        {
            _context = socialContext;
        }

        public async Task<ProfileModel> AddProfiles(List<TblPersonalInfo> info, List<tblUsersInfo> userinfos)
        {
            try
            {
                var result = await GetUserProfile(userinfos.First().UserId.ToString(), true);
                bool check = true;
                int i = 0;
                foreach (var item in info)
                {

                    foreach (var r in result.values)
                    {
                        if (r[0] == item.InfoType || r[1] == item.Value)
                        {
                            check = false;
                            break;
                        }
                    }
                    if (check)
                    {
                        await _context.TblPersonalInfo.AddAsync(item);
                        await _context.TblUsersInfo.AddAsync(userinfos[i]);
                        await _context.SaveData();
                    }
                    i++;
                }



                return await GetUserProfile(userinfos.First().UserId.ToString().ToLower(), true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
        public async Task<bool> DeleteProfile(string userid, string profileInfoID)
        {
            var userinfo =await _context.TblUsersInfo.AsNoTracking().Where(u => u.UserId.ToString().ToLower() == userid.ToLower() && u.PersonalInfoId.ToString().ToLower() == profileInfoID.ToLower()).FirstOrDefaultAsync();
            _context.TblUsersInfo.Remove(userinfo);
            _context.TblPersonalInfo.Remove(await _context.TblPersonalInfo.AsNoTracking().Where(u => u.PersonalInfoId == userinfo.PersonalInfoId).FirstOrDefaultAsync());
            var result = await _context.SaveData();
            return result > 0;
        }

        public async Task<ProfileModel> GetUserProfile(string userid, bool isUser = false)
        {
            ProfileModel model = new ProfileModel();
            var info = await _context.TblUsers.AsNoTracking().Where(u => u.UserId.ToString().ToLower() == userid.ToLower())
                .Include(i => i.TblUsersInfo).ThenInclude(ui => ui.TblPersonalInfo)
                .Select(a => a.TblUsersInfo.Where(t => userid.ToLower() == t.UserId.ToString().ToLower()).Select(e =>e.TblPersonalInfo))
                .ToListAsync();
            if (info != null) { 
            foreach (var detail in info)
            {
                foreach(var test in detail)
                {
                    if ((!isUser && test.IsPublic) || isUser)
                    {
                        model.values.Add(new string[] { test.InfoType, test.Value, test.IsPublic ? "true" : "false",test.PersonalInfoId.ToString().ToLower()});
                    }
                }
               
            }
            }


            return model;
        }

        public async Task<ProfileModel> UpdateProfiles(List<TblPersonalInfo> list, List<tblUsersInfo> infolist, List<bool> succeeded)
        {
            try
            {
                var result = await GetUserProfile(infolist.First().UserId.ToString(), true);
                bool check = true;
                int i = 0;
                foreach (var item in list)
                {

                    
                    if (check)
                    {
                        _context.TblPersonalInfo.Update(item);
                        var lines=await _context.SaveData();
                        if (lines == 0)
                        {
                            succeeded[i] = false;
                        }
                    }
                    i++;
                }

                var u = await GetUserProfile(infolist.First().UserId.ToString().ToLower(), true);
                u.SucceedList = succeeded;
                return u;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}
