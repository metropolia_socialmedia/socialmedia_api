﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;

namespace SocialMedia.API.Repos
{
    public interface IFriendRepo
    {
        Task<FriendModel> AddFriendAsync(string userid, string friendid);
        Task<FriendModel> GetFriendById(string userid, string friendid);
        Task<FriendModel> GetPublicUserById(string userid);
        Task<List<FriendModel>> GetUsersAsync(string userId);
        Task<int> RemoveFriendAsync(Guid userGuid, Guid friendGuid);
    }
}