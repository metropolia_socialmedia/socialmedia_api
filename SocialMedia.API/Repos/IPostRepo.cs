﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;
using SocialMedia.Models;

namespace SocialMedia.API.Repos
{
    public interface IPostRepo
    {
        Task<PostModel> AddPostAsync(Guid userid, TblPosts post);
        Task<int> DeletePostAsync(Guid postid);
        Task<List<PostModel>> GetPostsAsync(List<FriendModel> friends, int count, int page,string userid);
    }
}