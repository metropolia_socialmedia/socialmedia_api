﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SocialMedia.API.Models;
using SocialMedia.Models;

namespace SocialMedia.API.Repos
{
    public interface IUserRepo
    {
        Task<UserModel> ExtendTokens(string id, string refreshtoken);
        Task<List<TblUsers>> GetTestUsers();
        Task<List<UserModel>> GetUserByEmail(string email);
        Task<List<UserModel>> GetUserByUsername(string username);
        Task<string> HasValidToken(string token);
        Task<bool> HasValidToken(string id, string token);
        Task<TblUsers> RegisterUser(TblUsers newUser);
        Task<bool> UpdateUser(TblUsers tbluser);
        Task<bool> UpdateUser(UserModel user);
    }
}