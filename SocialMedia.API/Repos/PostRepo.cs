﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.API.Models;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Repos
{
    public class PostRepo : IPostRepo
    {
        Isocial_mediaContext _context;
        public PostRepo(Isocial_mediaContext context)
        {
            _context = context;
        }

        public async Task<List<PostModel>> GetPostsAsync(List<FriendModel> friends, int count, int page,string userid)
        {
            List<PostModel> posts = new List<PostModel>();

            foreach (var friend in friends)
            {
                posts.AddRange(await _context.TblUserPosts.Where(up => up.UserId.ToString().ToLower()==friend.User.userid.ToString().ToLower()).Include(up => up.Posts).Select(up => new PostModel { Owner = friend.User, text = up.Posts.Text, img = up.Posts.Image.Image, Postedat = up.Posts.PostedAt, postid=up.PostId.ToString().ToLower() }).Skip(page).Take(count).ToListAsync());
            }
            posts.AddRange(await _context.TblUserPosts.Where(up=>up.UserId.ToString().ToLower()==userid.ToLower()).Include(up => up.Posts).Include(up=>up.Users).Select(up => new PostModel { Owner = new UserModel {username=up.Users.Username,userid=up.Users.UserId.ToString().ToLower()}, text = up.Posts.Text, img = up.Posts.Image.Image, Postedat = up.Posts.PostedAt, postid = up.PostId.ToString().ToLower() }).Skip(page).Take(count).ToListAsync());
            posts = posts.OrderByDescending(o=>o.Postedat).Skip(page * count).Take(count).ToList();
            return posts;
        }
        public async Task<PostModel> AddPostAsync(Guid userid, TblPosts post)
        {
            post.PostId = Guid.NewGuid();
            post.PostedAt = DateTime.Now;
            await _context.TblPosts.AddAsync(post);
            await _context.TblUserPosts.AddAsync(new tblUserPosts { UserId = userid, PostId = post.PostId });
            await _context.SaveData();
            return await _context.TblUserPosts.Where(up => up.PostId == post.PostId).Include(up => up.Posts).ThenInclude(p => p.Image).Include(up => up.Users).Select(up => new PostModel
            {

                Owner = new UserModel
                {
                    userid = up.Users.UserId.ToString().ToLower(),
                    username = up.Users.Username
                },
                text = up.Posts.Text,
                Postedat = up.Posts.PostedAt,
                img = up.Posts.Image.Image,
                postid=up.PostId.ToString().ToLower()
            }).FirstOrDefaultAsync();
        }
        public async Task<int> DeletePostAsync(Guid postid)
        {
            var post = await _context.TblPosts.AsNoTracking().Where(up => up.PostId == postid).Include(up => up.Posts).FirstOrDefaultAsync();
            var userpost = post.Posts.Where(p => p.PostId == postid).FirstOrDefault();
            _context.TblUserPosts.Remove(userpost);
            _context.TblPosts.Remove(post);
            return await _context.SaveData();

        }

    }
}
