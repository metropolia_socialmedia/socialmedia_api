﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.API.Models;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Repos
{
    public class FriendRepo : IFriendRepo
    {
        Isocial_mediaContext _context;
        public FriendRepo(Isocial_mediaContext context)
        {
            _context = context;
        }

        public async Task<List<FriendModel>> GetUsersAsync(string userId)
        {
            var result = await _context.TblUserFriends.Where(f => f.userid.ToString().ToLower() == userId.ToLower())
                .Include(i => i.User).Select(f => new FriendModel { Id = f.id, User = new UserModel { userid = f.User.UserId.ToString(), username = f.User.Username } }).ToListAsync();
            return result;
        }
        public async Task<FriendModel> GetFriendById(string userid, string friendid)
        {
            return await _context.TblUserFriends.Where(f => (f.userid.ToString().ToLower() == userid.ToLower() && friendid.ToString().ToLower() == friendid.ToLower())
            || (f.userid.ToString().ToLower() == friendid.ToLower() && f.friendid.ToString().ToLower() == userid.ToLower())).Include(f => f.User).Select(u => new FriendModel
            {
                Id = u.id,
                User = new UserModel
                {
                    userid = u.User.UserId.ToString().ToLower(),
                    username = u.User.Username
                }
            }).FirstOrDefaultAsync();
        }
        public async Task<FriendModel> AddFriendAsync(string userid, string friendid)
        {
            if (Guid.TryParse(userid, out Guid UserGuid) && Guid.TryParse(friendid, out Guid friendGuid))
            {
                var friend = new TblFriends { userid = UserGuid, friendid = friendGuid };
                _context.TblUserFriends.Add(friend);
                _context.TblUserFriends.Add(new TblFriends { userid = friendGuid, friendid = UserGuid });
                await _context.SaveData();
                return await GetFriendById(userid, friendid);
            }
            return null;
        }

        public async Task<int> RemoveFriendAsync(Guid userGuid, Guid friendGuid)
        {
            var friend = await _context.TblUserFriends.AsNoTracking().Where(f => (f.userid == userGuid && f.friendid == friendGuid)
             || (f.userid == userGuid && f.friendid == userGuid)).ToListAsync();
            _context.TblUserFriends.RemoveRange(friend);
            return await _context.SaveData();
        }

        public async Task<FriendModel> GetPublicUserById(string userid)
        {
            var user = await _context.TblUsers.Where(f => f.UserId.ToString().ToLower() == userid.ToLower()).Include(f => f.TblUsersInfo).ThenInclude(ui => ui.TblPersonalInfo).Select(u => new FriendModel { User = new UserModel { username = u.Username,userid=u.UserId.ToString().ToLower() } }).FirstOrDefaultAsync();
            return user;
        }
    }
    
}
