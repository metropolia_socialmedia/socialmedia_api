﻿using Microsoft.EntityFrameworkCore;
using SocialMedia.API.Models;
using SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMedia.API.Repos
{
    public class UserRepo : IUserRepo
    {
        public const int TOKEN_TIME = 1;
        public const int REFRESH_TOKEN_TIME = 5;
        Isocial_mediaContext _smContext;
        public UserRepo(Isocial_mediaContext smContext)
        {
            _smContext = smContext;
        }
        public async Task<List<TblUsers>> GetTestUsers()
        {
            return await _smContext.TblUsers.ToListAsync();
        }

        public async Task<List<UserModel>> GetUserByUsername(string username)
        {
            return await _smContext.TblUsers.Where(user => user.Username == username).Select(u => new UserModel() { userid = u.UserId.ToString(), username = u.Username, email = u.Email, password = u.Password, refreshtoken = u.Refreshtoken != null ? u.Refreshtoken.ToString() : "", refreshtokenEndTime = u.RefreshtokenEndTime, token = u.Token != null ? u.Token.ToString() : "", tokenEndTime = u.TokenEndTime }).ToListAsync();
        }
        public async Task<List<UserModel>> GetUserByEmail(string email)
        {
            return await _smContext.TblUsers.Where(user => user.Email == email).Select(u => new UserModel() { userid = u.UserId.ToString(), username = u.Username, email = u.Email, password = u.Password, refreshtoken = u.Refreshtoken != null ? u.Refreshtoken.ToString() : "", refreshtokenEndTime = u.RefreshtokenEndTime, token = u.Token != null ? u.Token.ToString() : "", tokenEndTime = u.TokenEndTime }).ToListAsync();

        }
        private async Task<TblUsers> getUserByEmail(string email)
        {
            return await _smContext.TblUsers.Where(user => user.Email == email).FirstOrDefaultAsync();

        }
        public async Task<bool> HasValidToken(string id, string token)
        {
            var user = await _smContext.TblUsers.Where(usr => usr.UserId.ToString() == id && usr.Token.ToString() == token && usr.TokenEndTime > DateTime.Now).FirstOrDefaultAsync();
            return user != null;
        }
        public async Task<UserModel> ExtendTokens(string id, string refreshtoken)
        {
            var user = await _smContext.TblUsers.Where(usr => usr.UserId.ToString().ToLower() == id && usr.Refreshtoken.ToString().ToLower() == refreshtoken && usr.RefreshtokenEndTime >= DateTime.Now).FirstOrDefaultAsync();
            if (user == null)
            {
                return null;
            }
            user.Refreshtoken = Guid.NewGuid();
            user.Token = Guid.NewGuid();
            var check = await UpdateUser(user);
            return check ? new UserModel
            {
                email = user.Email,
                password = null,
                refreshtoken = user.Refreshtoken.ToString(),
                refreshtokenEndTime = user.RefreshtokenEndTime,
                token = user.Token.ToString(),
                tokenEndTime = user.TokenEndTime,
                username = user.Username
            } : null;

        }

        public async Task<bool> UpdateUser(UserModel user)
        {

            var tbluser = await getUserByEmail(user.email);
            tbluser.Refreshtoken = Guid.Parse(user.refreshtoken);
            tbluser.RefreshtokenEndTime = user.refreshtokenEndTime;
            tbluser.Token = Guid.Parse(user.token);
            tbluser.TokenEndTime = user.tokenEndTime;
            _smContext.TblUsers.Update(tbluser);
            var result = await _smContext.SaveData();
            return result > 0;
        }
        public async Task<bool> UpdateUser(TblUsers tbluser)
        {

            tbluser.RefreshtokenEndTime = DateTime.Now.AddHours(REFRESH_TOKEN_TIME);
            tbluser.TokenEndTime = DateTime.Now.AddHours(TOKEN_TIME);
            _smContext.TblUsers.Update(tbluser);
            var result = await _smContext.SaveData();
            return result > 0;
        }
        public async Task<TblUsers> RegisterUser(TblUsers newUser)
        {
            await _smContext.TblUsers.AddAsync(newUser);
            await _smContext.SaveData();
            var user = await _smContext.TblUsers.Where(u => u.UserId == newUser.UserId).FirstOrDefaultAsync();
            return user;
        }

        public async Task<string> HasValidToken(string token)
        {
            var user = await _smContext.TblUsers.Where(usr => usr.Token.ToString() == token && usr.TokenEndTime > DateTime.Now).Select(r=> r.UserId.ToString()).FirstOrDefaultAsync();
            return user;
        }
    }

}
