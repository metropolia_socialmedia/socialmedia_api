﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.API.Models;
using SocialMedia.API.Services;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecognitionController : ControllerBase
    {
        IUserService _uService;
        IRecognitionService _rService;
        IFriendService _fService;
        public RecognitionController(IUserService userService,IRecognitionService recognitionService,IFriendService friendService)
        {
            _uService = userService;
            _rService = recognitionService;
            _fService = friendService;
        }
        [HttpPost]
        public async Task<ActionResult> GetLinkedProfile([FromHeader] string Authorization, ImageModel image)
        {
            try
            {
                if (image == null || string.IsNullOrEmpty(image.base64))
                {
                    return new BadRequestResult();
                }
                var userid = await _uService.CheckToken(Authorization);
                if (string.IsNullOrEmpty(userid))
                {
                    return new UnauthorizedResult();
                }
                var user = await _rService.GetUser(image, userid);
                if (string.IsNullOrEmpty(user))
                {
                    return new NotFoundResult();
                }
                return new OkObjectResult(await _fService.GetPublicProfile(user));
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpPost("add")]
        public async Task<ActionResult> AddRecognitionImages([FromHeader] string Authorization,List<ImageModel> images)
        {
            try { 
            if (images == null || images.Count == 0)
            {
                return new BadRequestResult(); 
            }
            var userid =await _uService.CheckToken(Authorization);
            if (string.IsNullOrEmpty(userid))
            {
                return new UnauthorizedResult();
            }
                new RecognitionService();
                if (await _rService.ProcessLearning(images, userid))
                {
                    return new OkResult();
                }
                else
                {
                    return new StatusCodeResult(500);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }


        }
    }
}