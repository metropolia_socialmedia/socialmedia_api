﻿using Microsoft.AspNetCore.Mvc;
using SocialMedia.API.Models;
using SocialMedia.API.Services;
using System;
using System.Threading.Tasks;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        IUserService _userService;
        IProfileService _profileService;
        public ProfileController(IUserService userService, IProfileService profileService)
        {
            _userService = userService;
            _profileService = profileService;
        }

        private async Task<string> CheckToken(string token)
        {
            return await _userService.CheckToken(token);
        }

        [HttpGet]
        public async Task<ActionResult> GetUserProfile([FromHeader]string Authorization, [FromQuery]string ProfileId = null)
        {
            try
            {
                if (Authorization == null)
                {
                    return new BadRequestResult();
                }
                string userid = await CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                return new OkObjectResult(await _profileService.GetProfile(userid, ProfileId));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddUserProfile([FromHeader] string Authorization, ProfileModel info)
        {
            try
            {
                if (Authorization == null || info == null)
                {
                    return new BadRequestResult();
                }
                string userid = await CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                return new OkObjectResult(await _profileService.AddProfile(userid, info));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpPut]
        public async Task<ActionResult> EditUserProfile([FromHeader] string Authorization, ProfileModel info)
        {
            try
            {
                if (Authorization == null || info == null)
                {
                    return new BadRequestResult();
                }
                string userid = await CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                return new OkObjectResult(await _profileService.UpdateProfile(userid, info));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpDelete]
        public async Task<ActionResult> DeleteUserProfile([FromHeader] string Authorization, [FromQuery]string ProfileInfoID)
        {
            try
            {
                if (Authorization == null || ProfileInfoID == null)
                {
                    return new BadRequestResult();
                }
                var userid = await CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                if (await _profileService.DeleteProfile(userid, ProfileInfoID))
                {
                    return new OkResult();
                }
                return new NotFoundResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
    }
}