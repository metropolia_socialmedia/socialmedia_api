﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.API.Models;
using SocialMedia.API.Services;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendController : ControllerBase
    {
        IFriendService _service;
        IUserService _userService;

        public FriendController(IFriendService service,IUserService userService)
        {
            _service = service;
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> AddFriend([FromHeader] string Authorization, UserModel friend)
        {
            try { 
            var userid = await _userService.CheckToken(Authorization);
            if (userid!=null&&friend!=null&&!String.IsNullOrWhiteSpace(friend.userid))
            {
                var result =await _service.AddFriendAsync(userid, friend.userid);
                return new OkObjectResult(result);
            }
            return new BadRequestResult();
            }catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpDelete]
        public async Task<ActionResult> RemoveFriend([FromHeader] string Authorization,[FromQuery]string FriendId)
        {
            try
            {
                var userid = await _userService.CheckToken(Authorization);
                if (userid != null && !String.IsNullOrWhiteSpace(FriendId))
                {
                    var result = await _service.DeleteFriendAsync(userid, FriendId);
                    if (result > 0)
                    {
                        return new OkResult();
                    }
                    else if (result == 0)
                    {
                        return new NotFoundResult();
                    }
                 }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetFriends([FromHeader] string Authorization)
        {
            try
            {
                var userid = await _userService.CheckToken(Authorization);
                if (userid != null)
                {
                    var result = await _service.GetUserAsync(userid);
                    return new OkObjectResult(result);
                }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
    }
}