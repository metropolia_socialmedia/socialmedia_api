﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using SocialMedia.API.Models;
using SocialMedia.API.Services;
using System;
using System.Threading.Tasks;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }
        [HttpPost("Login")]
        public async Task<ActionResult> Login(UserModel user)
        {
            try
            {
                var username = "";
                if (user.username != null)
                {
                    username = user.username;
                }
                else if (user.email != null)
                {
                    username = user.email;
                }
                else
                {
                    return new BadRequestResult();
                }
                var result = await _service.LoginUser(username, user.password);
                if (result != null)
                {
                    return new OkObjectResult(result);
                }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Token")]
        public async Task<ActionResult> Token(UserModel user)
        {
            try
            {
                StringValues key;
                if (Request.Headers.TryGetValue("Authorization", out key))
                {
                    var result = await _service.RefreshToken(user.userid, key.ToString());
                    if (result != null)
                    {
                        return new OkObjectResult(result);
                    }
                    else
                    {
                        return new StatusCodeResult(401);
                    }
                }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpPost("Register")]
        public async Task<ActionResult> Register(UserModel newUser)
        {
            try
            {
                var user = await _service.RegisterUser(newUser);
                if (user == null)
                {
                    return new StatusCodeResult(417);
                }
                return new OkObjectResult(user);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
    }
}