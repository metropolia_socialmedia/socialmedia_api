﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.API.Models;
using SocialMedia.API.Services;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        IImageService _iService;
        IUserService _uService;
        public ImageController(IImageService imageService, IUserService userService)
        {
            _iService = imageService;
            _uService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> AddImage([FromHeader] string Authorization, ImageModel image)
        {
            try { 
            if (Authorization == null)
            {
                return new UnauthorizedResult();
            }
            if (image == null || string.IsNullOrWhiteSpace(image.base64))
            {
                return new BadRequestResult();
            }
            var userid = await _uService.CheckToken(Authorization);
            if (userid == null)
            {
                return new UnauthorizedResult();
            }
                return new OkObjectResult(await _iService.addImage(userid, image));
            }catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }


        }
        [HttpGet]
        public async Task<ActionResult> GetImages([FromHeader] string Authorization)
        {
            try
            {
                if (Authorization == null)
                {
                    return new UnauthorizedResult();
                }
             
                var userid = await _uService.CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                return new OkObjectResult(await _iService.GetImages(userid));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }


        }
        [HttpDelete]
        public async Task<ActionResult> RemoveImage([FromHeader] string Authorization,[FromQuery] string imageid)
        {
            try
            {
                if (Authorization == null)
                {
                    return new UnauthorizedResult();
                }
                if (string.IsNullOrWhiteSpace(imageid))
                {
                    return new BadRequestResult();
                }
                var userid = await _uService.CheckToken(Authorization);
                if (userid == null)
                {
                    return new UnauthorizedResult();
                }
                
                return new OkObjectResult(await _iService.RemoveImage(userid,imageid));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }


        }
    }
}