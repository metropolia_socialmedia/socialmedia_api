﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialMedia.API.Models;
using SocialMedia.API.Services;

namespace SocialMedia.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        IUserService _uService;
        IPostService _pService;
        public PostController(IUserService userService,IPostService postService)
        {
            _uService = userService;
            _pService = postService;
        }
        [HttpGet("user")]
        public async Task<ActionResult> GetUserPostFeed([FromHeader]string Authorization, [FromQuery] int max = 25, [FromQuery]int page = 0)
        {
            try
            {
                if (max > 25)
                {
                    return new BadRequestResult();
                }
                var userid = await _uService.CheckToken(Authorization);
                if (!string.IsNullOrWhiteSpace(userid))
                {
                    return new OkObjectResult(await _pService.GetUserPostsAsync(userid, max, page));
                }
                return new UnauthorizedResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetPostFeed([FromHeader]string Authorization, [FromQuery] int max=25,[FromQuery]int page = 0)
        {
            try
            {
                if (max > 25)
                {
                    return new BadRequestResult();
                }
                var userid = await _uService.CheckToken(Authorization);
                if (!string.IsNullOrWhiteSpace(userid))
                {
                    return new OkObjectResult(await _pService.GetPostsAsync(userid, max, page));
                }
                return new UnauthorizedResult();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddPost([FromHeader]string Authorization,PostModel post)
        {
            try
            {
                var userid = await _uService.CheckToken(Authorization);
                if (!string.IsNullOrWhiteSpace(userid)&&post!=null)
                {
                    post.Postedat = DateTime.Now;
                    return new OkObjectResult(await _pService.AddPostAsync(userid,post));
                }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }
        [HttpDelete]
        public async Task<ActionResult> DeletePost([FromHeader]string Authorization, [FromQuery]string postid)
        {
            try
            {
                var userid = await _uService.CheckToken(Authorization);
                if (!string.IsNullOrWhiteSpace(userid) &&  !string.IsNullOrWhiteSpace(postid))
                {
                    return new OkObjectResult(await _pService.DeletePost(postid));
                }
                return new BadRequestResult();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new StatusCodeResult(500);
            }
        }

    }
}